import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forRoot([
            // only define app wide global routes here modules specific routes will go in their own route files
            {
                path: '',
                loadChildren: './artists/artists.module#ArtistsModule'
            }
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutesModule {}
