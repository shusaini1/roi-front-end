import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {PageEvent} from '@angular/material';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  @Input() length: number;
  @Input() pageIndex: number = 0;
  @Input() pageSize: number = 9;
  @Input() pageSizeOptions: number[] = [9, 18, 45, 90];
  @Output() pageEvent: EventEmitter<PageEvent> = new EventEmitter<PageEvent>();

  constructor() { }

  ngOnInit() {
  }

  setPage(event:PageEvent) {
    this.pageEvent.emit(event);
  }

}
