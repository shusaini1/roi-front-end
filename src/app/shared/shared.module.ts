import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationComponent } from './components/pagination/pagination.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpCache } from './services/core/http-cache';
import { MatPaginatorModule } from '@angular/material/paginator';
import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';

@NgModule({
  imports: [CommonModule, HttpClientModule, MatPaginatorModule, LoadingModule.forRoot({
    animationType: ANIMATION_TYPES.wanderingCubes,
    backdropBackgroundColour: 'rgba(0,0,0,0.1)', 
    backdropBorderRadius: '4px',
    primaryColour: '#ffffff', 
    secondaryColour: '#ffffff', 
    tertiaryColour: '#ffffff'
  })],
  declarations: [PaginationComponent],
  exports: [PaginationComponent, HttpClientModule, LoadingModule]
})
export class SharedModule { 
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [HttpClient, HttpCache]
    }
  }
}