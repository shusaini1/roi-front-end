import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import { HttpCache } from './http-cache';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
    constructor(private _cache:HttpCache) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const jsonReq = req.clone({headers: req.headers.set('Content-Type', 'application/json')});

    if( req.method !== 'GET' ) {
        return next.handle(jsonReq);    
    }
    
    const cachedResponse = this._cache.get(req);
    if (cachedResponse) {
      // A cached response exists. Serve it instead of forwarding
      // the request to the next handler.
      return Observable.of(cachedResponse);
    }
    return next.handle(jsonReq).do((event) => {
        if( event instanceof HttpResponse ) {
            this._cache.put(jsonReq, event);
        }
    });
  }


}