import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class ArtistsService {
    serviceUrl = environment.apiBaseUrl+'?format=json&api_key='+environment.apiKey;

    constructor(private _httpClient:HttpClient) { }

    /**
     * Returns the list of artists
     * @param page 
     * @param limit 
     */
    getArtists(page:number = 1, limit:number = 5) {
        return this._httpClient.get(this.serviceUrl+`&method=geo.gettopartists&country=spain&page=${page}&limit=${limit}`);
    }

    getArtistByName(artistName:string) {
        return this._httpClient.get(this.serviceUrl+`&method=artist.getinfo&artist=${artistName}`);
    }

    getLikedArtists() {
        return JSON.parse(localStorage.getItem('user_likes'));
    }

    tagArtist(artist, tag) {
        let userTags = JSON.parse(localStorage.getItem('user_tags')) || [];
        let existIdx = -1;
        userTags.map((tags, idx) => {
            if( tags.mbid == artist.mbid ) {
                existIdx = idx;
            }
        });

        if( userTags.filter(item=> item.mbid == artist.mbid).length == 0 ) {
            artist.tags = tag;
            userTags.push(artist);
        } else if( existIdx > -1 ) {
            userTags[existIdx].tags = tag;
        }
        localStorage.setItem('user_tags', JSON.stringify(userTags));
        return true;
    }

    removeTag(artist) {
        let userTags = JSON.parse(localStorage.getItem('user_tags')) || [];
        let existIdx = -1;
        userTags.map((tags, idx) => {
            if( tags.mbid == artist.mbid ) {
                existIdx = idx;
            }
        });

        if( existIdx > -1 ) {
            userTags.splice(existIdx,1);
        }
        localStorage.setItem('user_tags', JSON.stringify(userTags));
        return true;
    }

    likeArtist(artist) {
        let userLikes = JSON.parse(localStorage.getItem('user_likes')) || [];
        let existIdx = -1;
        userLikes.map((like, idx) => {
            if( like.mbid == artist.mbid ) {
                existIdx = idx;
            }
        });
        let rtn = null;
        if( userLikes.filter(item=> item.mbid == artist.mbid).length == 0 ) {
            artist.is_liked = true;
            userLikes.push(artist);
            rtn = 'liked';
        } else if( existIdx > -1 ) {
            userLikes.splice(existIdx, 1);
            rtn = 'unliked';
        }
        localStorage.setItem('user_likes', JSON.stringify(userLikes));
        return rtn;
    }
}