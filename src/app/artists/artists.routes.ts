import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { DetailComponent } from './components/detail/detail.component';
import { LikedComponent } from './components/liked/liked.component';
import { ArtistsComponent } from './components/artists/artists.component';

@NgModule({
    imports:[RouterModule.forChild([
        {
            path: '',
            component: ArtistsComponent,
            children: [
                {
                    path: '',
                    component: HomeComponent,
                },
                {
                    path: 'likes',
                    component: LikedComponent,
                },
                {
                    path: 'artist/:name',
                    component: DetailComponent,
                }
            ]
        },
    ])],
    exports: [RouterModule]
})

export class ArtistsRoutesModule {};