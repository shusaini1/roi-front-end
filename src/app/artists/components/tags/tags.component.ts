import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ArtistsService } from '../../services/artists.service';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent {

  public tagForm:FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required])
  });

  constructor(
    public dialogRef: MatDialogRef<TagsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _artistsService:ArtistsService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(value, valid) {
    this._artistsService.tagArtist(this.data.artist, value.name);
    this.dialogRef.close({tag: value.name});
  }

}
