import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArtistList } from '../../models/artist-list.model';
import { ArtistsService } from '../../services/artists.service';
import { TagsComponent } from '../tags/tags.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  artists:ArtistList[] = [];
  page = 1;
  limit = 9;
  total = 0;
  loading: boolean;
  constructor(private _router: Router, private _activatedRoute:ActivatedRoute,private _artistService:ArtistsService,
    private _dialog:MatDialog) { }

  ngOnInit() {
    this._activatedRoute.queryParams.subscribe((params) => {
      this.page = params['page'] || 1;
      this.limit = params['limit'] || 9;
      this.initComponent();
    });
  }

  openDialog(artist) {
    let dialogRef = this._dialog.open(TagsComponent, {
      width: '250px',
      data: { artist: artist }
    });

    
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        let idx = this.artists.indexOf(artist);
        if( idx > -1 ) {
          this.artists[idx].tags = result.tag;
        }
      }
    });
  }

  removeTag(artist) {
    this._artistService.removeTag(artist);
    let idx = this.artists.indexOf(artist);
    if( idx > -1 ) {
      this.artists[idx].tags = '';
    } 
  }

  initComponent() {
    this.loading = true;
    this._artistService.getArtists(this.page, this.limit).subscribe(resp => {
      this.loading = false;
      this.total = resp['topartists']['@attr']['total'];
      this.artists = resp['topartists']['artist'].map((item) => {
        return new ArtistList(item);
      });
    });
  }

  setPage(event) {
    this.loading = true;
    this._artistService.getArtists((event.pageIndex + 1), (event.pageSize)).subscribe(resp => {
      this.total = resp['topartists']['@attr']['total'];
      this.artists = resp['topartists']['artist'].map((item) => {
        return new ArtistList(item);
      });
      this._router.navigate(['/'], {queryParams: {page: event.pageIndex+1}, replaceUrl: true});
      this.loading = false;
    });
  }

  likeArtist(artist) {
    let idx = this.artists.indexOf(artist);
    if( this._artistService.likeArtist(artist) == 'liked' ) {
      this.artists[idx].is_liked = true;
    } else {
      this.artists[idx].is_liked = false;
    }
  }
}
