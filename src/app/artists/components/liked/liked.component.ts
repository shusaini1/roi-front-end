import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArtistList } from '../../models/artist-list.model';
import { ArtistsService } from '../../services/artists.service';

@Component({
  selector: 'app-liked',
  templateUrl: './liked.component.html',
  styleUrls: ['./liked.component.css']
})
export class LikedComponent implements OnInit {
  artists:ArtistList[] = [];
  loading: boolean;
  constructor(private _artistService:ArtistsService) { }

  initComponent() {
    this.loading = true;
    let userLikes = this._artistService.getLikedArtists();
    if( userLikes ) {
      this.artists = userLikes.map((item) => {
        return new ArtistList(item);
      });
    }
    this.loading = false;
  }

  unlikeArtist(artist) {
    let idx = this.artists.indexOf(artist);
    if( this._artistService.likeArtist(artist) == 'unliked' ) {
      this.initComponent();
    }
  }

  ngOnInit() {
    this.initComponent();
  }

}
