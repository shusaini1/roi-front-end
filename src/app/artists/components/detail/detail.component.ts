import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Artist } from '../../models/artist.model';
import { ArtistsService } from '../../services/artists.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  name:string;
  artist: Artist;
  loading: boolean;
  error: any;
  constructor(private _activatedRoute:ActivatedRoute,private _artistService:ArtistsService) { }

  ngOnInit() {
    this._activatedRoute.params.subscribe((params) => {
      this.name = params['name'];
      this.getDetails();
    });
  }

  getDetails() {
    this.loading = true;
    this._artistService.getArtistByName(encodeURIComponent(this.name)).subscribe(data => {
        this.loading = false;
        if( _.has(data, 'artist') ) {
          this.artist = new Artist(data['artist']);
        } else {
          this.error = data['error'];
        }
        
    }, error => {
      console.log(error);
    });
  }

}
