import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetailComponent } from './components/detail/detail.component';
import { LikedComponent } from './components/liked/liked.component';
import { ArtistsService } from './services/artists.service';
import { ArtistsRoutesModule } from './artists.routes';
import { MatCardModule, MatIconModule, MatToolbarModule, MatButtonModule, MatDialogModule, MatFormFieldModule, 
  MatInputModule, MatChipsModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { NavbarComponent } from './navbar/navbar.component';
import { ArtistsComponent } from './components/artists/artists.component';
import { TagsComponent } from './components/tags/tags.component';


@NgModule({
  imports: [CommonModule, ArtistsRoutesModule, MatCardModule, MatIconModule, MatButtonModule, MatToolbarModule, 
    MatDialogModule, MatFormFieldModule, MatInputModule, SharedModule.forRoot(), FormsModule, ReactiveFormsModule,
    MatChipsModule
  ],
  declarations: [HomeComponent, DetailComponent, LikedComponent, NavbarComponent, ArtistsComponent, TagsComponent],
  providers: [ArtistsService],
  entryComponents: [TagsComponent]
})
export class ArtistsModule { }