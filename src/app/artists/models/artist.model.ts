export class Artist {
    mbid: string;
    name: string;
    thumbnail: {};
    listeners: string;
    playcount: string;
    bio: string;
    is_liked: boolean;
    tags:string;

    constructor(artist: any) {
        this.mbid = artist['mbid'] || '';
        this.name = artist['name'] || '';
        this.listeners = artist['stats']['listeners'] || '';
        this.playcount = artist['stats']['playcount'] || '';
        this.bio = artist['bio']['content'] || '';
        this.thumbnail = artist['image'][5]['#text'] || {thumbnail: 'http://homewardboundaz.org/wp-content/uploads/2016/09/person-placeholder-300x300.jpg'};
        this.is_liked = false;
        let userLikes = JSON.parse(localStorage.getItem('user_likes'));
        let userTags = JSON.parse(localStorage.getItem('user_tags'));
        if( userLikes ) {
            userLikes.map((userLike) => {
                if( userLike['mbid'] == artist['mbid'] ) {
                    this.is_liked = true;
                }
            });
        }

        if( userTags ) {
            userTags.map((tag) => {
                if( tag['mbid'] == artist['mbid'] ) {
                    this.tags = tag.tags;
                }
            })
        }
    }
}
