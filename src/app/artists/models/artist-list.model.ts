export class ArtistList {
    mbid: string;
    name: string;
    thumbnail: {};
    listeners: string;
    is_liked: boolean;
    tags: string;

    constructor(data: any) {
        this.name = data.name || '';
        this.mbid = data.mbid || '';
        if( data.thumbnail ) {
            this.thumbnail = data.thumbnail;
        } else {
            this.thumbnail = data.image[3]['#text'] || {thumbnail: 'http://homewardboundaz.org/wp-content/uploads/2016/09/person-placeholder-300x300.jpg'}
        }
        this.listeners = data.listeners || '';
        this.is_liked = false;
        this.tags = '';
        let userLikes = JSON.parse(localStorage.getItem('user_likes'));
        let userTags = JSON.parse(localStorage.getItem('user_tags'));
        if( userLikes ) {
            userLikes.map((userLike) => {
                if( userLike['mbid'] == data['mbid'] ) {
                    this.is_liked = true;
                }
            });
        }

        if( userTags ) {
            userTags.map((tag) => {
                if( tag['mbid'] == data['mbid'] ) {
                    this.tags = tag.tags;
                }
            })
        }
    }
}
