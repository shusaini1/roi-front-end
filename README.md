# Demo can be found here (Please clear your browser local storage first)
`http://jbeeaquariums.com/oohlaladevtest/`

# OOHLALA Front End Coding Challenge
> The main goal of this challenge is to assess your knowledge of some of the core frameworks and patterns used at OOHLALA, plus allowing us to see how you architect your front end code

***


## The Challenge
Single Page Application (SPA) with at least three routes.

You get to pick what API you want to work with as long as it returns JSON data.

The application is a simple app with **at least** three routes:

*Home*  `/` GET on resources

*Details*  `/:resourceID` GET on a particular resource

*Likes*  `/likes`  List of liked resources

**Home** Execute a GET on your resource and paginate the results, either by using the API’s pagination option or mock it on the front end, we want to see at least 3 pages of results, the page number should be appended to the url (e.g. `/?page=2`) and when refreshing the page your code should take into account the page number and fetch the corresponding results. (You should also create a pagination component)

**Details** Simple GET by ID on resource, make sure you handle errors when a resource is not found

**Likes** List of liked resources

How to like a resource?

When liking a resource you should prompt the user to Tag it so we could filter it in the future… that means you need to create a Form and validate their input (a tag field is mandatory and can’t be blank)

- We want to be able to Add and Delete Likes
- The likes feature should be lazy loaded
- We want so see how you commit code to a branch, so commit often.
- Likes should be persistent on page refresh
- No DB is necessary you can use LocalStorage or SessionStorage

Architect your application so it is scalable and some of the code can be shared with future components, take advantage of Angular features such as dependency injection, services, shared modules.


### Technical Requirements:
- Typescript
- Angular +4.X
- Responsive UI
- Unit Test (Make sure they are passing)


### How to submit the challenge
You have 48 hours to submit your challenge start by forking this repo and once you are ready submit a PR with a link to the live demo in the README.md, any other information on how to run your code and or tests should also be provided here.

### Bonus Points (aka It will really help if you do some of these…)
- i18n (Multiple languages)
- Pull data from multiple APIs
- Offline mode (Service Worker)
- Protect some routes from unauthorized users?

We think 48 hours is plenty of time for this, so feel free to add your own personal touch to the project by adding more features :)

If you have any questions please feel free to contact us at andres@oohlalamobile.com

Happy Hacking!





